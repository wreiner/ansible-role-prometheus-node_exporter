# Ansible Role: node_exporter

Installs node-exporter for Prometheus monitoring.

## Mandatory variables

```
mgmt_ipaddress: "192.168.122.135"
```

## ToDo

- [ ] Open firewall port
  - [ ] firewalld
  - [ ] ufw
- [x] Make version configurable
- [x] Change paths to variables
